﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorkREST.DTOs
{
    //ROLE: QualityAssurance They
    //can change the description, replication
    //procedures, notes, and all properties as they need
    public class Defect_QA_CoU_DTO
    {
        public int DefectId { get; set; }
        public string Description { get; set; }
        public string ReplicationProcedures { get; set; }
        public string Notes { get; set; }
        public double UserPainIndex { get; set; }
        public System.DateTime DateFound { get; set; }
        public Nullable<System.DateTime> DateFixed { get; set; }
        public Nullable<int> AssignedToId { get; set; }
        public int ProjectId { get; set; }
        public Nullable<int> ReportedById { get; set; }
        public int WorkflowStepId { get; set; }

        
    }
}