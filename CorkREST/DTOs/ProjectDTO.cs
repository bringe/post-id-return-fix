﻿using CorkREST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorkREST.DTOs
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int CompanyId { get; set; }
        
        public string CompanyName { get; set; }
        
    }
}