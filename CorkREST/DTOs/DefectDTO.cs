﻿using System;


namespace CorkREST.DTOs
{ 
    public class DefectDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ReplicationProcedures { get; set; }
        public string Notes { get; set; }
        public double UserPainIndex { get; set; }
        public DateTime DateFound { get; set; }
        public DateTime? DateFixed { get; set; }
        public int? AssignedToId { get; set; }
        public int ProjectId { get; set; }
        public int? ReportedById { get; set; }
        public int WorkflowStepId { get; set; }
    }
}