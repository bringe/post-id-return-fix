﻿using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using CorkREST.Authentication;
using CorkREST.Models;
using CorkREST.DTOs;

namespace CorkREST.Areas.API.Controllers
{
    [ValidateByApiKey]
    public class WorkflowStepController : BaseApiController
    {
        private DtoFactory dfac = new DtoFactory();
            public WorkflowStepController(Entities1 context) : base(context)
    {
        
    }

        // GET api/WorkflowStep
        /// <summary>
        /// Returns a list of Workflow Steps.
        /// </summary>
        /// <returns></returns>
        public IQueryable<WorkflowStepDTO> GetWorkflowSteps()
        {
            return _db.WorkflowSteps.AsQueryable<WorkflowStep>().Select(dfac.AsWorkFlowStepDTO);
           
           // return rtr;
        }

        // GET api/WorkflowStep/5
        /// <summary>
        /// Returns a specified Workflow Step
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(WorkflowStep))]
        public IHttpActionResult GetWorkflowStep(int id)
        {
            WorkflowStep workflowstep = _db.WorkflowSteps.Find(id);
            if (workflowstep == null)
            {
                return NotFound();
            }

            return Ok(workflowstep);
        }

        // PUT api/WorkflowStep/5
        /// <summary>
        /// Updates a specified Workflow Step.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="workflowstep"></param>
        /// <returns></returns>
        public IHttpActionResult PutWorkflowStep(int id, WorkflowStep workflowstep)
        {
            return StatusCode(HttpStatusCode.Forbidden);
        }

        // POST api/WorkflowStep
        /// <summary>
        /// Creates a new Workflow Step
        /// </summary>
        /// <param name="workflowstep"></param>
        /// <returns></returns>
        [ResponseType(typeof(WorkflowStep))]
        public IHttpActionResult PostWorkflowStep(WorkflowStep workflowstep)
        {
            return StatusCode(HttpStatusCode.Forbidden);
        }

        // DELETE api/WorkflowStep/5
        /// <summary>
        /// Deletes a specified Workflow Step.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(WorkflowStep))]
        public IHttpActionResult DeleteWorkflowStep(int id)
        {
            return StatusCode(HttpStatusCode.Forbidden);
        }

        private bool WorkflowStepExists(int id)
        {
            return _db.WorkflowSteps.Count(e => e.Id == id) > 0;
        }
    }
}