﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using CorkREST.Authentication;
using CorkREST.Models;
using System.Security.Cryptography;
using System.Web.Helpers;
using CorkREST.DTOs;
using System.Web.Http.Controllers;
using System.Web;
using System.Collections.Generic;

namespace CorkREST.Areas.API.Controllers
{
    [ValidateByApiKey]
    public class UserController : BaseApiController
    {
        private DtoFactory dfac = new DtoFactory();

    public UserController(Entities1 context) : base(context)
    {
        
    }

        //Example 1
        //[RoutePrefix("orders")]
        //public class OrdersController : ApiController
        //{
        //    [Route("{id}")]
        //    public Order Get(int id) { }
        //    [Route("{id}/approve")]
        //    public Order Approve(int id) { }
        //} 

        // Example 2
        //[Route("movies")]
        //public IEnumerable<Movie> Get() { }
        //[Route("actors/{actorId}/movies")]
        //public IEnumerable<Movie> GetByActor(int actorId) { }
        //[Route("directors/{directorId}/movies")]
        //public IEnumerable<Movie> GetByDirector(int directorId) { } 


        // Example 3
        // Optional parameter
        //   [Route("people/{name?}")]
        // Default value
        //  [Route("people/{name=Dan}")]
        // Constraint: Alphabetic characters only. 
        //   [Route("people/{name:alpha}")]

        // GET api/User
        /// <summary>
        /// Returns a List of Users
        /// </summary>
        /// 
        
        public IQueryable<ReturnUserDTO> GetUsers()
        {
            if (!IsSystemAdmin())
            {
                return CorkUser.Companies.SelectMany(c => c.Users).AsQueryable<User>().Select(dfac.AsReturnUserDTO);
            }

            return _db.Users.Include(u => u.Roles).Select(dfac.AsReturnUserDTO);
        }


        // GET api/User
        /// <summary>
        /// Returns the specified user 
        /// </summary>
        /// <param name="id">The ID of the user.</param>
        // GET api/User/5
        [ResponseType(typeof(ReturnUserDTO))]
        public IHttpActionResult GetUser(int id)
        {
            if (!CanAccessUser(id))
            {
                return Unauthorized();
            }

            ReturnUserDTO user = _db.Users.Include(u => u.Roles)
                .Where(u => u.Id == id)
                .Select(dfac.AsReturnUserDTO)
                .FirstOrDefault();
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        /// <summary>
        /// Resets the password for a specific user.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="passwordDto"></param>
        /// <returns></returns>
        [AuthenticateByRole("CompanyAdmin")]
        [Route("api/user/{userId:int}/setpassword")]
        public IHttpActionResult PutSetPassword(int userId, PasswordDto passwordDto)
        {
         
            if (!passwordDto.Password.Equals(passwordDto.Confirm)) 
            {
                return BadRequest();
            }

            var userFromDB = _db.Users.FirstOrDefault(u => u.Id == userId);

            if (userFromDB == null) 
            {
                return BadRequest();
            }

            if (!CanAccessCompany(userId)) 
            {
                return Unauthorized();
            }

            userFromDB.Password = Crypto.HashPassword(passwordDto.Password);

            _db.Entry(userFromDB).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(userId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }


            return Ok();
        }


        // PUT api/User/5
        /// <summary>
        /// Updates a specified user thorugh role or company.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [AuthenticateByRole("CompanyAdmin")]
        public IHttpActionResult PutUser(int id, UserPutDTO user)
        {
            //var requesterId = int.Parse(HttpContext.Current.Request.Headers.GetValues(HeaderValues.UserId).First());
            //var requester = db.Users.Find(requesterId);
            //var companyAdd = new Company();

            //if (user.AddToCompanyId != null)
            //{
            //    companyAdd = db.Companies.FirstOrDefault(co => co.Id == user.AddToCompanyId);
            //    if (companyAdd != null)
            //    {
            //        if (requester.Companies.Contains(companyAdd))
            //        {

            //        }
            //    }
            //}

            if (id != user.Id)
            {
                return BadRequest();
            }
            var userFromDB = _db.Users.FirstOrDefault(u => u.Id == user.Id);
            if (userFromDB == null)
            {
                return NotFound();
            }
            // remove user from company
            if (user.RemoveFromCompanyId != null)
            {
                var companyToRemoveFrom = _db.Companies.Find(user.RemoveFromCompanyId);
                if (companyToRemoveFrom == null || !companyToRemoveFrom.Users.Contains(userFromDB))
                {
                    return BadRequest();
                }
                else
                    companyToRemoveFrom.Users.Remove(userFromDB);
            }
            //remove user from role
            if (user.RemoveFromRoleId != null)
            {
                var roleToRemoveFrom = _db.Roles.Find(user.RemoveFromRoleId);
                if (roleToRemoveFrom == null || !roleToRemoveFrom.Users.Contains(userFromDB))
                {
                    return BadRequest();
                }
                else
                    roleToRemoveFrom.Users.Remove(userFromDB);
            }
            //add user to company
            if (user.AddToCompanyId != null)
            {
                var companyToAddToo = _db.Companies.Find(user.AddToCompanyId);
                if (companyToAddToo == null || companyToAddToo.Users.Contains(userFromDB))
                {
                    return BadRequest();
                }
                else
                    companyToAddToo.Users.Add(userFromDB);
            }
            // add user to role
            if (user.AddToRoleId != null)
            {
                var roleToAddToo = _db.Roles.Find(user.AddToRoleId);
                if (roleToAddToo == null || roleToAddToo.Users.Contains(userFromDB))
                {
                    return BadRequest();
                }
                else
                    roleToAddToo.Users.Add(userFromDB);
            }


            if (user.LastName != null)
                userFromDB.LastName = user.LastName;
            if (user.FirstName != null)
                userFromDB.FirstName = user.FirstName;
            if (user.Email != null)
                userFromDB.Email = user.Email;

            _db.Entry(userFromDB).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        // POST api/User
        /// <summary>
        /// Createws a new user.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [AuthenticateByRole("CompanyAdmin")]
        [ResponseType(typeof(ReturnUserDTO))]
        public IHttpActionResult PostUser(UserDTO user)
        {
            if (user.FirstName == null || user.LastName == null || user.LastName == null || user.Password == null)
            {
                return BadRequest();
            }

            if (!CanAccessCompany(user.AssignToCompanyId))
            {
                return Unauthorized();
            }


            var companyToAssignToo = _db.Companies.Find(user.AssignToCompanyId);

            if (companyToAssignToo == null && user.AssignToRoleId != 1)
            {
                return BadRequest();
            }

            var roleToAssignToo = _db.Roles.Find(user.AssignToRoleId);
            if (roleToAssignToo == null)
            {
                return BadRequest();
            }

            User toStore = new User();
            toStore.ApiKey = GetApiKey();
            toStore.Password = Crypto.HashPassword(user.Password);
            toStore.LastName = user.LastName;
            toStore.FirstName = user.FirstName;
            toStore.Email = user.Email;
            if (companyToAssignToo != null)
                companyToAssignToo.Users.Add(toStore);
            roleToAssignToo.Users.Add(toStore);

            _db.Users.Add(toStore);

            _db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = toStore.Id }, user);
        }

        // DELETE api/User/5
        /// <summary>
        /// Deletes a specified user.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(User))]
        public IHttpActionResult DeleteUser(int id)
        {
            User user = _db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            _db.Users.Remove(user);
            _db.SaveChanges();

            return Ok(user);
        }

        private static string GetApiKey()
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                var bytes = new byte[16];
                rng.GetBytes(bytes);
                return Convert.ToBase64String(bytes);
            }
        }

        private bool UserExists(int id)
        {
            return _db.Users.Count(e => e.Id == id) > 0;
        }
    }
}