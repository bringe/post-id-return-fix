﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorkREST.Authentication
{
    public class HeaderValues
    {
        public const string UserId = "x-cmps-383-authentication-id";
        public const string HmacDigest = "x-cmps-383-authentication-digest";
        public const string Email = "x-cmps-383-authentication-email";
        public const string Password = "x-cmps-383-authentication-password";
        public const string Key = "x-cmps-383-authentication-key";
    }
}