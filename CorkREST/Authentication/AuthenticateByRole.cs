﻿using CorkREST.Areas.API.Controllers;
using CorkREST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace CorkREST.Authentication
{
    public class AuthenticateByRole : AuthorizeAttribute
    {
        private List<string> rolesAuthorized;
        private BaseApiController controller;
        private User user;

        public AuthenticateByRole(string roles)
        {
            rolesAuthorized = new List<string>();

            if (roles != null)
            {
                roles.Trim(' ');
                rolesAuthorized.AddRange(roles.Split(','));
            }

            if (Roles != null)
            {
                Roles.Trim(' ');
                rolesAuthorized.AddRange(Roles.Split(','));
            }
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            controller = (BaseApiController)actionContext.ControllerContext.Controller;
            user = controller.CorkUser;

            var userRoles = user.Roles.Select(r => r.Name);

            if (userRoles.Contains("SystemAdmin"))
            {
                return;
            }

            int userId = user.Id;

            foreach (var role in rolesAuthorized)
            {
                if (userRoles.Contains(role))
                {
                    return;
                }
            }

            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "You are not authorized.");
            return;
        }

    }
}
